1
00:00:00,000 --> 00:00:01,768
<i>Previously on Vikings...</i>

2
00:00:01,769 --> 00:00:02,838
처음 번역하는거라 

3
00:00:02,839 --> 00:00:04,067
많이 어색함 그니까

4
00:00:04,072 --> 00:00:05,332
닥치고 보렴 

5
00:00:05,340 --> 00:00:08,342
어색한거 찾을정도면 영자막으로 봐

6
00:00:08,343 --> 00:00:09,973
You have seen all you needed to?

7
00:00:09,978 --> 00:00:12,578
They are not as safe
as they think they are.

8
00:00:12,580 --> 00:00:14,080
The greatest army
ever assembled.

9
00:00:14,082 --> 00:00:15,682
Our instruments of wrath.

10
00:00:15,683 --> 00:00:17,243
Ivar wants to lead the army.

11
00:00:17,251 --> 00:00:18,811
He thinks our father chose him.

12
00:00:18,820 --> 00:00:20,253
Bjorn should lead the army.

13
00:00:20,254 --> 00:00:21,514
I am the leader
of this great army.

14
00:00:21,522 --> 00:00:23,022
Do you understand that?

15
00:00:23,024 --> 00:00:24,854
You are about to receive
a great heathen army.

16
00:00:24,859 --> 00:00:26,628
They are here.
The great heathen army

17
00:00:26,629 --> 00:00:29,366
come to revenge the death
of Ragnar Lothbrok.

18
00:00:29,367 --> 00:00:31,024
My lord bishop,
let us go to war!

19
00:00:31,032 --> 00:00:32,192
God help us!

20
00:00:32,200 --> 00:00:33,767
I don't think he can.

21
00:00:33,768 --> 00:00:35,538
Ragnar!

22
00:00:37,537 --> 00:00:39,277
This is where
our father was killed.

23
00:01:35,595 --> 00:01:37,725
Yeah, we need those reinforced!

24
00:01:37,732 --> 00:01:38,992
Bring some more!

25
00:01:41,034 --> 00:01:43,304
The stake needs retying!

26
00:01:47,074 --> 00:01:49,443
Every other man
on the outer perimeter!

27
00:01:49,444 --> 00:01:51,674
Look at the towers. You'll see.

28
00:01:53,113 --> 00:01:54,173
Gate!

29
00:02:40,127 --> 00:02:41,895
저사람들은 누구지?

30
00:02:41,896 --> 00:02:43,126
저도 잘 모르겠습니다.

31
00:02:45,265 --> 00:02:48,395
다른 상인들이랑 오늘아침에 도착했는데,

32
00:02:48,402 --> 00:02:51,602
살펴보기만 하고는 아무것도 안샀습니다.

33
00:02:51,606 --> 00:02:54,806
저는 저자신에게 물었죠, "왜 어떤 물건에도 관심이 없으면서,
34
00:02:54,809 --> 00:02:57,209
"저 먼길을 왔을까?" 하고요.

35
00:03:01,849 --> 00:03:03,519
공격하라!

36
00:03:48,862 --> 00:03:50,692
저들이 뭐한거지?

37
00:03:50,698 --> 00:03:52,798
우리 방어전략의 약점을 찾기위해.

38
00:03:53,900 --> 00:03:55,270
하나는 찾은거 같네.

39
00:04:00,373 --> 00:04:01,603
문을 열어라!

40
00:04:21,361 --> 00:04:23,221
사제님이 왕 에일라와 함께 전투에 참여했습니다.

41
00:04:24,931 --> 00:04:26,261
신이시여...

42
00:04:26,267 --> 00:04:28,267
지금 사제님은 하나님께 다가서고 있습니다.

43
00:04:28,269 --> 00:04:29,839
당신보다 더요, 애설울프 왕자.

44
00:04:34,107 --> 00:04:35,177
사제님을 눕혀라.

45
00:04:40,113 --> 00:04:41,413
누가 사제님에게 이런짓을 했습니까?

46
00:04:43,584 --> 00:04:45,584
라그나 로스브로크의 아들이었나요?

47
00:04:45,586 --> 00:04:47,586
그들의 군대.
그들의 군대는 얼마나 강력했습니까?

48
00:04:47,588 --> 00:04:49,456
얼마나 많은 전사들이 있었나요?

49
00:04:49,457 --> 00:04:51,687
얼마나 많은 잔디 잎이었는가...

50
00:04:53,760 --> 00:04:55,270
그들이 평야에 있었나요?

51
00:04:56,062 --> 00:04:57,362
그게 무슨 뜻 입니까?

52
00:04:57,365 --> 00:04:59,295
백명? 천명?

53
00:04:59,300 --> 00:05:01,034
말해주세요, 빨리.

54
00:05:01,035 --> 00:05:02,305
말하라고!!!

55
00:05:05,272 --> 00:05:06,502
씨발!

56
00:05:09,743 --> 00:05:11,843
하.. 그리고 명복을 빕니다.

57
00:05:20,654 --> 00:05:23,684
사제님께 적절한 교회장을 치루어드리게.

58
00:05:25,325 --> 00:05:28,355
힘든시간일지라도,
우리는 반드시 옳은일을 해야하네.

59
00:05:33,667 --> 00:05:36,067
그럼, 에일라 왕이 졌군.

60
00:05:36,070 --> 00:05:38,980
가장 끔찍하고 야만적인 죽음이었습니다, 폐하.

61
00:05:38,981 --> 00:05:40,403
그렇게 말했습니다.

62
00:05:41,574 --> 00:05:43,344
그래, 그거참 유감이군.

63
00:05:45,345 --> 00:05:46,845
우리 모두에게.

64
00:05:49,582 --> 00:05:52,752
엄청난 군대가 의심할 여지 없이 이미 여기로 향하고있다.

65
00:05:52,753 --> 00:05:56,353
난 너를 보낼것이다, 내아들아,
그들에게 맞서도록.

66
00:05:56,357 --> 00:05:58,627
만약 그들이 비숍이 말한만큼 강력하다면,

67
00:05:58,628 --> 00:06:00,956
군대를 징집할 시간이 필요합니다.

68
00:06:00,961 --> 00:06:03,621
이해한다. 그러나 지금 시간이 그렇게
많지 않다, 아들아.

69
00:06:03,631 --> 00:06:05,361
그들은 짧은시간내에 무기를, 병력을, 배를,

70
00:06:05,366 --> 00:06:07,336
웨섹스까지 가져오지 못합니다.

71
00:06:07,337 --> 00:06:09,435
설령 그렇다 해도, 시간은

72
00:06:09,437 --> 00:06:10,837
가장 중요한 것이다.
우리는...

73
00:06:10,838 --> 00:06:12,178
아버지, 제게 명령을 내렸으면,

74
00:06:12,179 --> 00:06:13,603
제가 알아서 하게 두세요!

75
00:06:14,474 --> 00:06:16,843
아버지, 저 아버지랑 같이 싸우고 싶어요.

76
00:06:16,844 --> 00:06:18,274
저도요.

77
00:06:20,880 --> 00:06:22,949
난 너희들이 목숨을 걸게하고 싶지 않구나.

78
00:06:22,950 --> 00:06:25,118
너희들은 미래야.

79
00:06:25,119 --> 00:06:26,489
그리고 내 임무는 너희들을 지키는거고.

80
00:06:26,490 --> 00:06:28,757
가러가렴. 안심하고 자려무나.

81
00:06:33,293 --> 00:06:35,623
착하구나, 착해.

82
00:06:39,566 --> 00:06:41,735
신이 너와함께 빈다,
아들아.

83
00:06:41,736 --> 00:06:44,836
지금, 내가 꿈꿧던, 계획했던 모든게

84
00:06:44,839 --> 00:06:47,374
가장큰 위험에 처했구나.

85
00:06:47,375 --> 00:06:50,245
아버지만이 꿈을 꾸는 유일한 사람이
아닙니다.

86
00:06:50,246 --> 00:06:52,914
용서해 주신다면,전령을 보내도록 하겠습니다.
( 이후 두지문영자막이 짤림)

87
00:07:00,086 --> 00:07:04,724
다시한번 그에게 돌아가라고 말해주고 싶구나
, 내사랑.

88
00:07:04,725 --> 00:07:07,855
내 아들은 너의 힘과, 지혜를 필요로 한단다.

89
00:07:07,862 --> 00:07:10,892
내 아들를 용서해야해,
내 아들이 널 용서할수 있도록.

90
00:07:10,898 --> 00:07:12,766
동물처럼 죽어가는 나에게

91
00:07:12,767 --> 00:07:15,097
애착을 가질 필요없는 없단다.

92
00:07:16,903 --> 00:07:17,963
제가 원하지 않는다면요?

93
00:07:21,107 --> 00:07:22,747
그렇다면 난 너에게 명령을 내릴수 밖에 없구나

94
00:07:23,943 --> 00:07:24,943
너의 왕을 대려오라고.

95
00:07:29,249 --> 00:07:30,519
My lady.

96
00:07:34,254 --> 00:07:35,824
왜 잠이 않오니?

97
00:07:36,389 --> 00:07:37,629
두렵니?

98
00:07:38,124 --> 00:07:39,124
아뇨.

99
00:07:39,692 --> 00:07:42,122
- 전 두렵지 않아요.
- 좋아.

100
00:07:42,129 --> 00:07:43,529
넌 두려워할 필요가 없단다.

101
00:07:44,798 --> 00:07:46,868
네 아버지가 널 계속 지켜보고 있으니까.

102
00:07:48,468 --> 00:07:50,708
그렇지만 아버지가 제 아버지잖아요

103
00:07:53,139 --> 00:07:54,679
내말은, 네 친부 말이다.

104
00:07:56,109 --> 00:07:58,545
너도 잘알고 있겠지만.

105
00:07:58,546 --> 00:08:00,146
네 아버지의 이름은 애썰스탄이고 수도승이었단다.

106
00:08:01,648 --> 00:08:03,718
그는 매우 축복받은 사람이었어,
 

107
00:08:04,751 --> 00:08:06,551
그는 매우 특별한 사람이었어.

108
00:08:08,722 --> 00:08:11,152
그는 우리의 삶을 바꿔놨단다.

109
00:08:11,158 --> 00:08:13,398
너는 그런 아버지를 가진걸 자랑스러워 해야해

110
00:08:14,661 --> 00:08:16,861
또 저는 아버지같은 아버지를 둬서 자랑스럽구요.

111
00:08:34,247 --> 00:08:35,515
이쪽으로!

112
00:08:35,516 --> 00:08:36,916
빨리! 빨리!

113
00:08:38,184 --> 00:08:40,014
빨리! 빨리!

114
00:08:43,691 --> 00:08:46,251
노르만의 분노로부터,
신이시여, 우리를 지켜주소서!

115
00:08:47,761 --> 00:08:49,591
빨리! 계속 움직여!

116
00:09:44,688 --> 00:09:47,757
신이시여 그가 적에게 둘러쌓여
혼란스러워 한다면

117
00:09:47,758 --> 00:09:49,988
부디 자비를 베푸시고 지켜주시오솝소서

118
00:09:49,993 --> 00:09:52,493
그리고 그에게 성전의 승리를 안겨주시옵소서

119
00:09:54,864 --> 00:09:56,494
감사합니다, 신부님. 

120
00:09:58,434 --> 00:10:00,564
지금은 괜찮아 보이는구나.

121
00:10:00,571 --> 00:10:03,171
두려워 말거라.
우리는 금방 같이할꺼야.

122
00:10:04,607 --> 00:10:05,907
주님께 기도를.

123
00:10:08,611 --> 00:10:10,913
내 남편.

124
00:10:10,914 --> 00:10:14,344
우리의 사랑과 우리의 희망이 함께할꺼에요.

125
00:10:14,351 --> 00:10:15,585
무사하길.

126
00:10:15,586 --> 00:10:18,086
반드시, 반드시, 살아야해요!

127
00:10:18,088 --> 00:10:20,556
시도해 보겠소.

128
00:10:20,557 --> 00:10:23,157
그리고 당신을 지킬수 있도록 노력하겠소, 
주디스.

129
00:10:28,798 --> 00:10:32,234
모든게 저 시간안에 있단다,

130
00:10:32,235 --> 00:10:35,135
그리고 신께서 목적하신 모든것 또한.

131
00:10:35,138 --> 00:10:38,274
사랑할 시간,
그리고 증오할 시간.

132
00:10:38,275 --> 00:10:41,475
전쟁을 할 시간,
그리고, 평화의 시간도.

133
00:10:41,478 --> 00:10:44,647
지금은 전쟁의 시간이야!

134
00:10:44,648 --> 00:10:46,718
증오의 시간이고!!

135
00:11:04,667 --> 00:11:05,897
가자!

136
00:11:05,902 --> 00:11:07,262
가자!!! 

137
00:11:07,270 --> 00:11:08,670
전진!!!!

138
00:11:09,840 --> 00:11:11,080
대열을 맞춰!

139
00:11:34,630 --> 00:11:36,465
내가 본거같군

140
00:11:36,466 --> 00:11:39,266
그 색슨족년이 겁먹은걸

141
00:11:39,835 --> 00:11:41,965
그들은 약해 빠졌어.

142
00:11:41,972 --> 00:11:44,532
그놈들이 우리 상대가 될거같지가 않아.

143
00:11:44,541 --> 00:11:45,941
넌 그들에 대해 잘몰라, 아이바.

144
00:11:46,709 --> 00:11:48,179
넌 경험이 없잖아.

145
00:11:49,378 --> 00:11:50,948
그들은 용감해.

146
00:11:52,214 --> 00:11:54,314
난 그들에 맞서 싸운 경험이 있지만,
넌 없지.

147
00:11:54,317 --> 00:11:56,686
난 내가 본것만 말해, 비욘.

148
00:11:56,687 --> 00:12:00,356
그리고 내가 본건 겁먹은 놈들이 좆빠지게 도망치는거고.

149
00:12:00,357 --> 00:12:03,697
난 그 약해빠진 신이
우리의 신으로부터 도망치는걸 봤고

150
00:12:03,698 --> 00:12:05,224
한번,

151
00:12:05,228 --> 00:12:08,898
왜 연장자의 말을 듣지않는거냐, 현명한 동생님?

152
00:12:08,899 --> 00:12:09,899
저놈들이 도망치는건

153
00:12:09,900 --> 00:12:11,967
저들은 전사가 아니여서야.

154
00:12:11,968 --> 00:12:13,568
저놈들은 단 한번이라도
우리와 싸운적이 없어

155
00:12:13,570 --> 00:12:14,910
이 왕국을 지키기 위해.

156
00:12:14,911 --> 00:12:17,205
그리고 그들의 명예를 지키기 위해서도.

157
00:12:17,207 --> 00:12:18,907
전사한테 명예를 빼면 뭐가남지?

158
00:12:18,909 --> 00:12:21,677
난 모르겠는데.
말해줘봐, 형.

159
00:12:21,678 --> 00:12:22,808
그리고

160
00:12:22,813 --> 00:12:24,373
형은 얼마나 싸워봤는지?

161
00:12:25,748 --> 00:12:27,316
너랑같지, 동생아,

162
00:12:27,317 --> 00:12:29,987
그래도 난 그 좆같이 편안한 전차는 안타니까!

163
00:12:29,988 --> 00:12:31,756
뭘배웠니, 아이바,

164
00:12:31,757 --> 00:12:34,355
만약 너가 형제간의 유대를 깨버리면,

165
00:12:34,357 --> 00:12:35,925
우리는 이기지 못해.

166
00:12:35,926 --> 00:12:38,056
많은 도전들이 우리앞에 있어.

167
00:12:38,061 --> 00:12:39,530
그러니까, 만약 너가 계속 싸우고

168
00:12:39,531 --> 00:12:41,499
보지년들처럼 굴꺼면,

169
00:12:41,500 --> 00:12:42,767
난 너에게 떠나는걸 추천하고 싶다.

170
00:12:42,768 --> 00:12:43,936
우린 너가 필요없어.

171
00:12:43,937 --> 00:12:45,494
근데 형은 내가 필요할거야.

172
00:12:45,502 --> 00:12:46,692
왜 아버지가 날고른거같아?

173
00:12:46,703 --> 00:12:47,933
영국에 함께오는 사람으로?

174
00:12:47,938 --> 00:12:50,206
아버지는 그렇게한 이유가 있으셨어

175
00:12:50,207 --> 00:12:53,776
아버지는 내가 아버지를 위해
행동할 사람이라 했고,

176
00:12:53,777 --> 00:12:55,907
아버지의 복수를 확실히할 사람이라고 했어.

177
00:12:58,714 --> 00:13:00,714
그렇게 생각하고싶으면,

178
00:13:00,717 --> 00:13:02,347
그렇게 생각해.

179
00:13:06,555 --> 00:13:08,455
난 이해해 형이

180
00:13:08,458 --> 00:13:10,128
위대한 라그나 로스브루크의

181
00:13:10,129 --> 00:13:11,957
머리를 받아들이기 힘들었단걸

182
00:13:11,962 --> 00:13:14,322
장애인에 병신을 대신해서.

183
00:13:19,969 --> 00:13:21,369
그래서 이게 모든일에 대한

184
00:13:21,371 --> 00:13:22,871
작은 돼지들의 꿀꿀거림이냐.

185
00:13:43,626 --> 00:13:45,056
거기가 아니야!

186
00:13:46,061 --> 00:13:47,831
어디에다 둬야해?

187
00:13:51,401 --> 00:13:52,501
그가왔어!

188
00:13:53,602 --> 00:13:54,832
애썰울프 왕자님!

189
00:13:54,838 --> 00:13:56,068
저기에 계셔!

190
00:13:57,072 --> 00:13:58,672
애썰울프 왕자님!

191
00:14:18,427 --> 00:14:20,927
그들이 얼마나 되는거같나? 진심으로.

192
00:14:21,430 --> 00:14:22,830
누가 말해보겠는가?

193
00:14:23,833 --> 00:14:26,193
3천? 4천?

194
00:14:26,203 --> 00:14:28,363
그게 내가 예측하는 숫자라네.

195
00:14:28,371 --> 00:14:30,211
전 이런 광경을 한번도 본적이 없습니다.

196
00:14:30,212 --> 00:14:33,943
기마병이 없지만, 강력한 이교도의 군대입니다.

197
00:14:33,944 --> 00:14:35,704
어디로 향하고 있나?

198
00:14:35,712 --> 00:14:37,102
욜크 근처에서 에일라왕을 죽이고 

199
00:14:37,113 --> 00:14:38,543
렙톤을 앞에 두고 남쪽에서

200
00:14:38,548 --> 00:14:40,783
미드랜드로 향하고 있습니다.

201
00:14:40,784 --> 00:14:43,684
그들이 내 아버지의 왕국을 공격할거라 믿나?

202
00:14:43,687 --> 00:14:46,487
그녀석들이 뭉치고 뭉쳐서 웨색스를 공격할거라고?

203
00:14:46,489 --> 00:14:48,557
의심의 여지가 없습니다.

204
00:14:48,558 --> 00:14:51,158
그녀석들은 라그나 로스브루크의 아들입니다.

205
00:14:51,161 --> 00:14:53,501
그녀석들은 자기들 아버지의 복수를 원합니다.

206
00:14:53,502 --> 00:14:55,564
실제로 그들은 에일라왕을 죽였습니다.

207
00:14:55,565 --> 00:14:58,895
그리고 그녀석들은 왕께서 관련되있는걸 압니다.

208
00:14:58,902 --> 00:15:00,232
피할수 없어보입니다.

209
00:15:00,237 --> 00:15:02,307
그녀석들은 웨색스를 향해 진군하고 있습니다.

210
00:15:04,006 --> 00:15:05,706
자네를 믿네.

211
00:15:05,709 --> 00:15:07,476
난 언제나 생각했지

212
00:15:07,477 --> 00:15:10,713
그놈들이 내 아버지에게 복수하기를 원한다고

213
00:15:10,714 --> 00:15:13,244
자, 그럼 렙톤으로 이동하자고.

214
00:15:13,250 --> 00:15:15,320
우리의 운명이 우리를 거기서 기다리고 있을꺼야.

215
00:15:19,544 --> 00:15:20,774
타나루즈!

216
00:15:21,846 --> 00:15:23,414
플로키, 일어나!

217
00:15:23,415 --> 00:15:24,545
플로키!

218
00:15:24,550 --> 00:15:25,550
Hmm?

219
00:15:26,818 --> 00:15:28,788
- 뭔일이야?
- 사라졌어.

220
00:15:29,253 --> 00:15:30,753
타나루즈가 사라졌어.

221
00:15:32,925 --> 00:15:34,755
그게더 나을수도 있어.

222
00:15:35,860 --> 00:15:38,029
무슨생각을 하는거야?

223
00:15:38,030 --> 00:15:40,368
무언가에 겁먹고 도망친거야.

224
00:15:40,369 --> 00:15:43,434
그아이가 위험할 수 도 있어, 플로키!
플로키, 그아이를 찾아야해!

225
00:15:43,435 --> 00:15:45,265
아가야 어디있니?

226
00:15:47,772 --> 00:15:49,942
플로키, 찾아줘, 제발.

227
00:16:09,961 --> 00:16:11,131
타나루즈?

228
00:17:05,416 --> 00:17:06,486
내가 미안해.

229
00:17:09,153 --> 00:17:10,813
넌 우리를 싫어하지. 난...

230
00:17:14,425 --> 00:17:16,155
내가 뭘해야할지 모르겠다.

231
00:17:29,807 --> 00:17:32,037
타나루즈. 내 아가!

232
00:17:32,610 --> 00:17:33,880
내아가!

233
00:17:34,712 --> 00:17:36,852
괜찮지, 괜찮은거지?

234
00:17:39,150 --> 00:17:41,790
빨리, 빨리.

235
00:18:06,811 --> 00:18:08,081
그놈들이 여기있다!

236
00:18:50,087 --> 00:18:52,187
저 위쪽에! 문을 막아!

237
00:19:44,642 --> 00:19:46,082
회관으로 가자!

238
00:20:04,962 --> 00:20:06,062
아스트리드!

239
00:20:07,999 --> 00:20:09,669
여기가 아니야!

240
00:20:10,501 --> 00:20:12,101
톨비! 여기를 지켜.

241
00:20:14,805 --> 00:20:16,675
- 빨리!
- 네!

242
00:21:21,439 --> 00:21:22,609
싸우자!

243
00:22:00,578 --> 00:22:01,578
에길!

244
00:22:02,413 --> 00:22:03,573
도망쳐야해!

245
00:22:03,582 --> 00:22:04,752
빨리!

246
00:22:22,733 --> 00:22:24,063
그만!

247
00:22:35,513 --> 00:22:36,513
Ah!

248
00:22:40,985 --> 00:22:42,415
살려둬.

249
00:22:47,658 --> 00:22:49,188
도망쳐!

250
00:22:50,362 --> 00:22:51,502
후퇴!

251
00:23:25,613 --> 00:23:26,683
그만.

252
00:23:35,256 --> 00:23:36,356
맞아.

253
00:23:36,724 --> 00:23:37,724
인정할게.

254
00:23:38,860 --> 00:23:40,630
내가 멍청했어 저년을 죽였어야해.

255
00:23:42,597 --> 00:23:45,367
저년이 널 계속 기다릴거라 생각한
너가 멍청했어.

256
00:23:46,367 --> 00:23:48,697
저년이 널 좋아한다 생각했지? 멍청하긴.

257
00:23:48,704 --> 00:23:50,242
어떻게 아무것도 아닌 남자랑 결혼하지?

258
00:23:50,243 --> 00:23:51,369
아마 저년은 저놈을 좋아하나보지.

259
00:23:51,373 --> 00:23:52,643
그리고, 저년이 저놈을 좋아한다면,

260
00:23:54,142 --> 00:23:55,612
저놈은 누군가가 되는거고.

261
00:23:58,546 --> 00:24:00,606
어떻게 갑자기 똑똑해 진거냐, 동생아?

262
00:24:01,783 --> 00:24:03,383
아마 형이 신경안쓰는동안에?

263
00:24:03,385 --> 00:24:05,345
그리고 난 결혼을 포기 했어.

264
00:24:05,820 --> 00:24:07,120
왠지 알아 형?

265
00:24:08,590 --> 00:24:11,230
형보다 여자가 이해가 안가거든.

266
00:24:14,162 --> 00:24:15,662
여자들은 변하기 쉬워

267
00:24:17,499 --> 00:24:18,499
잊어버려.

268
00:24:35,517 --> 00:24:36,517
엘리시프?

269
00:24:39,187 --> 00:24:40,747
널 용서해주러 왔어.

270
00:24:44,092 --> 00:24:46,432
네 잘못이 아니야 날 기다리지 않은건.

271
00:24:47,862 --> 00:24:50,031
넌 아마 이게 불가능한 일이라고 생각했겠지

272
00:24:50,032 --> 00:24:52,267
어쨋든간에,

273
00:24:52,268 --> 00:24:53,728
난 결국 가정을 이루지 못했고.

274
00:24:55,537 --> 00:24:57,437
넌 나랑 결혼할 생각이 없지,

275
00:24:58,373 --> 00:24:59,373
그렇지?

276
00:25:00,308 --> 00:25:01,538
미안해.

277
00:25:03,044 --> 00:25:04,784
그러고보니 아직 자네의 이름도 모르는구만.

278
00:25:05,880 --> 00:25:08,280
제이름은 빅입니다, 하랄드 왕이시여.

279
00:25:08,950 --> 00:25:11,886
왕을 뵙게되어 기쁩니다.

280
00:25:11,887 --> 00:25:14,887
믿어주십시오,
난 이일에대해 아무것도...

281
00:25:14,890 --> 00:25:16,890
왕께서 제 아내와 맺은 협약을요.

282
00:25:17,458 --> 00:25:18,726
Mmm.

283
00:25:18,727 --> 00:25:21,557
그래도 기쁩니다, 왕께서 아내를 용서해주신다니

284
00:25:21,564 --> 00:25:22,804
Mmm.

285
00:25:23,998 --> 00:25:25,166
Hmm?

286
00:25:25,167 --> 00:25:26,627
안돼!

287
00:25:26,635 --> 00:25:27,635
빅!

288
00:25:42,150 --> 00:25:43,280
빅.

289
00:25:44,352 --> 00:25:45,752
미안하게 됐어.

290
00:26:08,443 --> 00:26:10,813
그들은 널 에길이라 불렀지!

291
00:26:12,413 --> 00:26:13,853
서자 에길.

292
00:26:15,383 --> 00:26:16,953
그게 그들이 널부르는 법이지.

293
00:26:19,754 --> 00:26:22,664
근데 넌 백작이 아니지

294
00:26:23,725 --> 00:26:24,755
왕도 아니고.

295
00:26:33,035 --> 00:26:35,365
- 근데 어떻게 너가 이런 군대를 모집했을까?

296
00:26:37,138 --> 00:26:38,708
누가 네 군대를 모집할 돈을 줬지,

297
00:26:39,540 --> 00:26:41,380
서자 에길?

298
00:26:42,076 --> 00:26:43,706
내가 왜 말해야 하지?

299
00:26:45,079 --> 00:26:46,648
넌 날 죽일꺼잖아

300
00:26:46,649 --> 00:26:48,549
내가 말하던 안하던
산채로 태워서

301
00:26:49,584 --> 00:26:51,624
- 그건 아니야.
- 그렇고 말고!!!

302
00:26:51,625 --> 00:26:54,150
장난 칠 생각마.

303
00:26:54,156 --> 00:26:56,556
너가 보듯이,
난 장난칠 시간이 없어.

304
00:26:58,226 --> 00:26:59,926
우린 네 아내를 찾았어.

305
00:27:14,142 --> 00:27:15,412
여기를 봐!

306
00:27:20,815 --> 00:27:22,445
우린 그녀에게 말했어

307
00:27:22,451 --> 00:27:25,321
만약 네가 우리에게 누가 네 공격에
돈을 대줬는지 말하면

308
00:27:26,654 --> 00:27:29,257
널 살려줄거라고.

309
00:27:29,258 --> 00:27:32,930
그럼넌 네 아내와 함께 살수있겠지,
행복하게.

310
00:27:34,796 --> 00:27:36,256
난 저 여자를 몰라.

311
00:27:39,034 --> 00:27:40,774
그냥 말해.

312
00:27:48,210 --> 00:27:49,580
그냥 말하라고, 에길

313
00:27:56,384 --> 00:27:58,294
만약 내가 말한다면, 널위한거야.

314
00:27:59,354 --> 00:28:01,054
내가 아니라.

315
00:28:01,056 --> 00:28:02,456
어쨋든 난 죽어.

316
00:28:03,725 --> 00:28:05,725
아마 널 살려줄꺼야.

317
00:28:13,000 --> 00:28:15,640
난 하랄드 왕에게 돈을 받았다.

318
00:28:25,213 --> 00:28:26,813
경계를 계속해라!

319
00:28:29,484 --> 00:28:30,924
자리를 지켜!

320
00:28:32,387 --> 00:28:33,547
자리!

321
00:28:37,658 --> 00:28:39,358
계속가!

322
00:28:39,361 --> 00:28:41,429
후방 빨리따라와!

323
00:28:41,430 --> 00:28:43,330
정신을 잃지마라!

324
00:29:18,299 --> 00:29:19,799
멈춰!

325
00:29:19,802 --> 00:29:21,302
멈춰!

326
00:29:21,303 --> 00:29:23,037
Whoa, whoa, whoa!

327
00:29:23,038 --> 00:29:24,038
할트!

328
00:29:26,241 --> 00:29:27,910
무슨일이지?

329
00:29:27,911 --> 00:29:30,181
색슨놈들이 하루면 닿을정도로 와 있습니다.

330
00:29:30,182 --> 00:29:31,779
그놈들 군대를 엄청 모아왔습니다.

331
00:29:33,582 --> 00:29:34,952
여기에 캠프를 만든다.

332
00:29:36,084 --> 00:29:37,684
내일 싸울수 있겠군.

333
00:29:37,687 --> 00:29:39,217
우리의 아버지의 이름아래에,

334
00:29:40,589 --> 00:29:41,989
우리는 승리할것이다.

335
00:29:44,661 --> 00:29:46,361
캠프를 만들어!

336
00:29:46,362 --> 00:29:48,102
여기에 캠프를 만들어라!

337
00:29:49,531 --> 00:29:52,067
형은 캠프를 여기에 만들어,

338
00:29:52,068 --> 00:29:54,898
난 우리가 싸울곳을 둘러보고 올테니까.

339
00:29:54,904 --> 00:29:56,564
뭔말 하는거냐?

340
00:29:56,573 --> 00:29:59,613
저놈들은 우리가 어떻게 싸우는지 예상하고 있어.

341
00:29:59,614 --> 00:30:02,109
우리가 왜 저들이 원하는대로 싸워야하지?

342
00:30:02,111 --> 00:30:04,611
왜 저놈들을 놀래킬

343
00:30:04,614 --> 00:30:05,974
다른방법으로 안싸우고?

344
00:30:11,119 --> 00:30:13,956
우리 전사들은 뭔일이 일어나는지
이해하지 못할거야.

345
00:30:13,957 --> 00:30:15,917
우리는 방패벽안에서 싸워
그게 우리가 싸우는 방식이고.

346
00:30:15,925 --> 00:30:18,355
그런데 지금 우리는 더 커다란 군대를 가졌잖아.

347
00:30:18,361 --> 00:30:20,861
그리고 저들도 똑같고, hvitserk.

348
00:30:20,863 --> 00:30:22,633
우리는 같은 방식으로 싸우지 않을거야.

349
00:30:22,634 --> 00:30:24,132
바꾸기에는 너무 늦었어.

350
00:30:24,133 --> 00:30:25,973
넌 누군데 그말 지껄이냐?
닥쳐봐.

351
00:30:25,974 --> 00:30:27,269
우린 네 형제야.

352
00:30:29,771 --> 00:30:31,011
함께!

353
00:30:34,743 --> 00:30:36,453
왜 전술을 바꾸길 원하는거냐?

354
00:30:36,454 --> 00:30:38,706
승리하고 싶어, 형?

355
00:30:44,152 --> 00:30:45,152
들어봐,

356
00:30:46,288 --> 00:30:47,718
함께가자, 비욘.

357
00:30:48,991 --> 00:30:51,491
전장을 조사하러 가자고.

358
00:30:51,494 --> 00:30:54,954
아마도, 우리는 좁고 작은 장소에서 싸우기보다

359
00:30:54,964 --> 00:30:56,254
더 큰 장소에서 싸워야해

360
00:30:56,265 --> 00:30:58,425
더 넒은 장소, 너 커다란 장소에서.

361
00:30:58,434 --> 00:31:00,264
그리고 지형을 이용해야지.

362
00:31:00,269 --> 00:31:02,739
도랑들, 언덕들, 나무들.

363
00:31:07,476 --> 00:31:08,736
뭐라할꺼야?

364
00:31:11,113 --> 00:31:12,213
만약 그게 통하면,

365
00:31:12,981 --> 00:31:14,581
좋은 계획이겠지.

366
00:31:16,284 --> 00:31:18,144
그렇지 않다면,

367
00:31:18,154 --> 00:31:19,684
나쁜계획이겠고.

368
00:31:32,367 --> 00:31:34,836
말을 함께 붙잡아!

369
00:31:34,837 --> 00:31:36,637
뭘기다리고 있냐?

370
00:31:36,639 --> 00:31:37,739
Ha!

371
00:31:56,858 --> 00:31:58,088
말해도 될까?

372
00:32:40,602 --> 00:32:41,802
말해.

373
00:32:43,705 --> 00:32:45,205
내가 실수했어.

374
00:32:45,208 --> 00:32:48,008
정말 미안해.
기다렸어야 했어.

375
00:32:48,010 --> 00:32:49,850
난 그를 사랑한적이없어. 널 사랑해.

376
00:32:50,545 --> 00:32:52,445
사람들이 날 부추긴거야.

377
00:32:52,448 --> 00:32:55,517
나한테 거짓말을 했어.
이해하지?

378
00:32:55,518 --> 00:32:57,688
네 꿈은 너무 긴시간이 걸릴것처럼 보였어.

379
00:33:00,622 --> 00:33:03,022
근데 내가 틀렸어, 진짜로,
난 기다렸어야 했어!

380
00:33:04,793 --> 00:33:07,796
그리고 이제 알았어.

381
00:33:07,797 --> 00:33:10,197
넌 네가 한번 말한건 반드시 이루어내.

382
00:33:12,467 --> 00:33:14,167
그리고 난 이제 널 믿어야해.

383
00:33:16,705 --> 00:33:18,165
이젠 널 믿어.

384
00:33:19,708 --> 00:33:21,008
용서해줘.

385
00:34:14,095 --> 00:34:17,465
보아하니, 이제부터,

386
00:34:17,466 --> 00:34:20,266
내가 형보다 여자를 잘아는거같네, 형!

387
00:34:58,826 --> 00:35:00,166
전진!

388
00:35:00,761 --> 00:35:02,091
계속 가라!

389
00:35:08,103 --> 00:35:09,503
계속 전진하라!

390
00:35:21,649 --> 00:35:23,549
깃발을 높게 들어!!

391
00:35:38,866 --> 00:35:40,206
계속 경계해!

392
00:36:06,595 --> 00:36:07,595
Ha!

393
00:36:17,272 --> 00:36:18,602
전진!

394
00:36:21,108 --> 00:36:22,208
대장!

395
00:36:22,210 --> 00:36:23,340
플랭크!

396
00:37:03,150 --> 00:37:04,350
전하!

397
00:37:13,428 --> 00:37:15,728
돌려!

398
00:37:17,232 --> 00:37:19,232
전부 돌려!

399
00:37:32,913 --> 00:37:34,243
저들을 쫒아라!

400
00:37:34,748 --> 00:37:35,848
계속가!

401
00:37:40,321 --> 00:37:41,421
계속 뭉쳐있어라!

402
00:37:49,330 --> 00:37:50,830
반대로!

403
00:37:50,832 --> 00:37:51,832
반대로! 반대!!!

404
00:37:54,034 --> 00:37:55,034
반대로!!!!

405
00:37:55,969 --> 00:37:56,969
반대로 돌려!!!!

406
00:37:59,139 --> 00:38:00,439
방패벽!

407
00:38:01,275 --> 00:38:02,475
방패벽!

408
00:38:02,477 --> 00:38:03,947
함께해라!!

409
00:38:08,015 --> 00:38:09,015
안쪽으로, 전하!

410
00:38:14,188 --> 00:38:15,388
전진!!!!

411
00:38:20,728 --> 00:38:22,488
뭉쳐라!!!!

412
00:39:20,429 --> 00:39:22,159
애썰울프 왕자님?

413
00:39:22,165 --> 00:39:23,535
애썰울프 왕자님?

414
00:39:47,122 --> 00:39:48,522
Mmm...

415
00:39:48,525 --> 00:39:49,865
다시 후퇴하자.

416
00:39:51,260 --> 00:39:52,690
네가 그렇게 말한다면.

417
00:40:08,510 --> 00:40:09,540
빨리!

418
00:40:31,466 --> 00:40:32,466
기다려!

419
00:40:44,179 --> 00:40:45,479
저기에 있습니다, 왕자님.

420
00:40:46,882 --> 00:40:47,882
기다려!

421
00:40:51,553 --> 00:40:52,553
Whoa.

422
00:41:00,262 --> 00:41:03,902
더이상 저새끼들한테 속아넘어가지 않을거다!

423
00:41:06,602 --> 00:41:08,402
너 저새끼들의 배가 렙톤에 남아있다고 했지?

424
00:41:08,404 --> 00:41:10,439
예, 애썰울프 왕자님.

425
00:41:10,440 --> 00:41:12,510
렙톤이 어느방향이지?

426
00:41:15,277 --> 00:41:17,607
그렇다면 저곳이 우리가 갈곳이다.

427
00:41:17,614 --> 00:41:21,617
그리고 렙톤에 도착하면 저들의 배를 먼저 부숴라

428
00:41:21,618 --> 00:41:23,778
그렇다면 우리는 이번 싸움보다
더 나은 싸움을 할 수 있을거야

429
00:41:23,786 --> 00:41:27,286
그전에 우리는 빨리움직여야 한다,
저새끼들을 뒤에 두고서!

430
00:41:27,290 --> 00:41:28,360
움직여!

431
00:41:36,365 --> 00:41:37,935
저새끼들 뭐하는거지?

432
00:41:39,034 --> 00:41:40,634
어디로 가는거야?

433
00:41:42,771 --> 00:41:44,301
렙톤, 내생각에는요.

434
00:41:50,979 --> 00:41:52,349
보트로?

435
00:41:53,482 --> 00:41:55,552
저새끼들 지금 우리 배로 가고있는거야?

436
00:41:57,486 --> 00:41:59,686
Oh,너이 병신 개새끼가!

437
00:42:00,289 --> 00:42:02,489
너가 맞았어!!!

438
00:42:02,492 --> 00:42:05,092
니가 맞았다고!!!!

439
00:42:05,094 --> 00:42:06,762
Oh, 이 미친 똘아이같은 천재새끼!!!

440
00:42:06,763 --> 00:42:07,763
니가 맞았다고!!!

441
00:42:30,919 --> 00:42:32,187
숙여!!!

442
00:42:32,188 --> 00:42:33,518
대형안으로 들어와!!!

443
00:42:34,289 --> 00:42:35,689
언덕으로 부터 방어해라!

444
00:42:47,269 --> 00:42:48,739
후퇴!!!!!

445
00:43:22,804 --> 00:43:24,074
방패벽!!!!

446
00:43:28,043 --> 00:43:29,483
돌격!!!

